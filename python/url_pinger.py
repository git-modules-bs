#!/usr/bin/python
import urllib2, time, threading, sys

class UrlFetchSample (threading.Thread):
        def __init__(self, urlAsString, fileName, samplingFrequency=5):
                threading.Thread.__init__(self)
                self.urlAsString = urlAsString
                self.fileName = fileName
                self.samplingFrequency = samplingFrequency
        def run(self):
                _file = open(self.fileName, "w")
                print "File Name: ", self.fileName, "\n"
                _file.write("URL: "+self.urlAsString+"\n")
                print "Probing: "+self.urlAsString, "\n"
                total_response_time=0
                total_duration=0
                success=0
                for index in range(self.samplingFrequency):
                        iter_file_name=self.fileName+"."+str(index)
                        print "Iter File Name: ", iter_file_name, "\n"
                        i_file = open(iter_file_name, "w")
                        i_file.write("Executing sample: "+str(index)+"\n")
                        print "Executing sample: " + self.fileName + " " + str(index), "\n"
                        start_time=time.time()
                        try:                        
                                f = urllib2.urlopen(urllib2.Request(self.urlAsString))
                                response_time=time.time()
                                if f.code != 200:
                                	i_file.write(f.code)
                                	i_file.write(" ")
                                	i_file.write(f.msg)
                                	i_file.write("\n")
                                else:
                                        data=f.read()
                                        data_time=time.time()
                                        i_file.write("Response time: " + str(response_time - start_time)+"\n")
                                        i_file.write("Date retrieval time: " + str(data_time - response_time)+"\n")
                                        i_file.write("Duration: " + str(data_time - start_time)+"\n")
                                        i_file.write(str(data))
                                        i_file.write("\n")
                                        total_response_time += (response_time - start_time)
                                        total_duration += (data_time - start_time)
                                        success += 1
                        except urllib2.HTTPError:
                                print "Error! " + self.fileName + " " + str(index) + " " + self.urlAsString, "\n"
                                pass
                if success > 0:
                        _file.write("Average Response Time: " + str(total_response_time/success) + "\n")
                        _file.write("Average Duration: " + str(total_duration/success) + "\n")
                _file.close()
fetchers=[]
_idx=0
for arg in sys.argv:
        if _idx > 0:
                print "URL: ", arg
                fetchers.append(UrlFetchSample(arg, "html-"+str(_idx)+".log", 3))
        _idx = _idx + 1
for fetcher in fetchers:
        fetcher.start()
        
