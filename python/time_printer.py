import time
import sys

for arg in sys.argv:
    _time = arg
print time.strftime("%Y-%m-%dT%H:%M:%S%z", time.gmtime((int(_time))))
